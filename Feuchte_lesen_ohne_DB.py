import time
import paho.mqtt.client as mqtt
import sys
import base64
import json


def saveToDB(someJSON):
    end_device_ids = someJSON["end_device_ids"]
    device_id = end_device_ids["device_id"]
    if device_id == 'tennis-feuchte':
        # application_id = end_device_ids["application_ids"]["application_id"]
        if "received_at" in someJSON:
            received_at = someJSON["received_at"]
            if "uplink_message" in someJSON:
                uplink_message = someJSON["uplink_message"]
                f_port = uplink_message["f_port"]
                f_cnt = uplink_message["f_cnt"]
                frm_payload = uplink_message["frm_payload"]
                frm_bytes = bytes(frm_payload, 'utf-8')
                t1 = base64.b64decode(frm_bytes)
                t2 = int.from_bytes(t1[0:1], "big")
                t3 = int.from_bytes(t1[1:2], "big")
                t4 = int.from_bytes(t1[2:3], "big")
                t5 = int.from_bytes(t1[3:5], "little")     # ADC 0
                print('t3: ', t3)
                if t3 > 127:
                    t3 = t3 - 256   # negative Feuchtewerte werden konvertiert
                # Kalibrierung nach neuem Einbau am 3.7.2023

                t3 = int(t3 * 99. / 112. + 70)
                t2 = t2 - 100       # Temperaturen sind um 100° nach oben versetzt
                # rufe alten Regenwert ab
                print(jetzt(), ' Nachricht erhalten: 0x', t1.hex(), '  Länge: ', len(t1))
                print('Temperatur: ', t2, '°c',  '  Feuchte: ', t3, '%', '  Regen: ', t4, '  Spannung: ', 3*t5)
                werte = [{'Id': 'Feuchte', 'value': t3}, {'Id': 'Temperatur', 'value': t2},
                         {'Id': 'Regen', 'value': t4}, {'Id': 'Spannung', 'value': t5}]

                #result = inhalt.insert_many(werte)

                rssi = uplink_message["rx_metadata"][0]["rssi"]
            elif "downlink_queued" in someJSON:
                downlink = someJSON["downlink_queued"]
                f_port = downlink["f_port"]
                frm_payload = downlink["frm_payload"]
                frm_bytes = bytes(frm_payload, 'utf-8')
                t1 = base64.b64decode(frm_bytes)
                t2 = int.from_bytes(t1, "big")
                print('Nachricht auf Port: ', f_port, ' erhalten: ', t1.hex(), '  Länge: ', len(t1))
            elif "downlink_sent" in someJSON:
                downlink = someJSON["downlink_sent"]
                f_port = downlink["f_port"]
                frm_payload = downlink["frm_payload"]
                frm_bytes = bytes(frm_payload, 'utf-8')
                t1 = base64.b64decode(frm_bytes)
                t2 = int.from_bytes(t1, "big")
                print('Nachricht auf Port: ', f_port, ' erhalten: ', t2, '  hex: ', t1.hex(), '  Länge: ', len(t1))
    if device_id == 'mein-2-testgeraet':
        # application_id = end_device_ids["application_ids"]["application_id"]
        if "received_at" in someJSON:
            received_at = someJSON["received_at"]
            if "uplink_message" in someJSON:
                uplink_message = someJSON["uplink_message"]
                f_port = uplink_message["f_port"]
                f_cnt = uplink_message["f_cnt"]
                frm_payload = uplink_message["frm_payload"]
                frm_bytes = bytes(frm_payload, 'utf-8')
                t1 = base64.b64decode(frm_bytes)
                t2 = int.from_bytes(t1[0:1], "big")
                t3 = int.from_bytes(t1[1:2], "big")
                t4 = int.from_bytes(t1[2:3], "big")
                t5 = int.from_bytes(t1[3:5], "little")     # ADC 0
                if t3 > 127:
                    t3 = t3 - 256   # negative Feuchtewerte werden konvertiert
                t2 = t2 - 100       # Temperaturen sind um 100° nach oben versetzt
                # rufe alten Regenwert ab
                print(jetzt(), 'Nachricht erhalten: 0x', t1.hex(), '  Länge: ', len(t1))
                print('Temperatur: ', t2, '°c',  '  Feuchte: ', t3,'%', '  Regen: ', t4, '  Spannung: ', t5)
                werte = [{'Id': 'Feuchte', 'value': t3}, {'Id': 'Temperatur', 'value': t2},
                         {'Id': 'Regen', 'value': t4}, {'Id': 'Spannung', 'value': t5}]

                #result = inhalt.insert_many(werte)

                rssi = uplink_message["rx_metadata"][0]["rssi"]
            elif "downlink_queued" in someJSON:
                downlink = someJSON["downlink_queued"]
                f_port = downlink["f_port"]
                frm_payload = downlink["frm_payload"]
                frm_bytes = bytes(frm_payload, 'utf-8')
                t1 = base64.b64decode(frm_bytes)
                t2 = int.from_bytes(t1, "big")
                print('Nachricht auf Port: ', f_port, ' erhalten: ', t1.hex(), '  Länge: ', len(t1))
            elif "downlink_sent" in someJSON:
                downlink = someJSON["downlink_sent"]
                f_port = downlink["f_port"]
                frm_payload = downlink["frm_payload"]
                frm_bytes = bytes(frm_payload, 'utf-8')
                t1 = base64.b64decode(frm_bytes)
                t2 = int.from_bytes(t1, "big")
                print('Nachricht auf Port: ', f_port, ' erhalten: ', t2, '  hex: ', t1.hex(), '  Länge: ', len(t1))
    if device_id == 'plastik-mobil':
        # application_id = end_device_ids["application_ids"]["application_id"]
        if "received_at" in someJSON:
            received_at = someJSON["received_at"]
            if "uplink_message" in someJSON:
                uplink_message = someJSON["uplink_message"]
                f_port = uplink_message["f_port"]
                f_cnt = uplink_message["f_cnt"]
                frm_payload = uplink_message["frm_payload"]
                frm_bytes = bytes(frm_payload, 'utf-8')
                t1 = base64.b64decode(frm_bytes)
                t2 = int.from_bytes(t1[0:1], "big")     # Temperatur
                t3 = int.from_bytes(t1[1:2], "big")     # Zähler
                t4 = int.from_bytes(t1[2:4], "little")     # ADC 0
                t5 = int.from_bytes(t1[4:6], "little")     # ADC 2

#                t2 = t2 - 100
                print(jetzt(), 'Nachricht erhalten: 0x', t1.hex(), '  Länge: ', len(t1))
                print('Temperatur: ', t2, '°c',  '  Zähler: ', t3, '  ADC 0: ', t4, '  ADC 2: ', t5)

                werte = [{'Id': 'ADC_0', 'value': t4}, {'Id': 'Plastik_T', 'value': t2},
                         {'Id': 'ADC_2', 'value': t5}]          #  nicht speichern!!!
#                result = inhalt.insert_many(werte)

                rssi = uplink_message["rx_metadata"][0]["rssi"]
                # snr = uplink_message["rx_metadata"][0]["snr"];
                #snr = 0
                #data_rate_index = uplink_message["settings"]["data_rate"]
                #consumed_airtime = uplink_message["consumed_airtime"]
            elif "downlink_queued" in someJSON:
                downlink = someJSON["downlink_queued"]
                f_port = downlink["f_port"]
                frm_payload = downlink["frm_payload"]
                frm_bytes = bytes(frm_payload, 'utf-8')
                t1 = base64.b64decode(frm_bytes)
                t2 = int.from_bytes(t1, "big")
                print('Nachricht auf Port: ', f_port, ' erhalten: ', t1.hex(), '  Länge: ', len(t1))
            elif "downlink_sent" in someJSON:
                downlink = someJSON["downlink_sent"]
                f_port = downlink["f_port"]
                frm_payload = downlink["frm_payload"]
                frm_bytes = bytes(frm_payload, 'utf-8')
                t1 = base64.b64decode(frm_bytes)
                t2 = int.from_bytes(t1, "big")
                print('Nachricht auf Port: ', f_port, ' erhalten: ', t2, '  hex: ', t1.hex(), '  Länge: ', len(t1))


# MQTT event functions
def on_connect(mqttc, obj, flags, rc):
    print("\nConnect: rc = " + str(rc))
    mqttc.subscribe("#", 0)
    print("subscribed")


def on_message(mqttc, obj, msg):
    print("\nMessage: " + msg.topic + " " + str(msg.qos))  # + " " + str(msg.payload))
    parsedJSON = json.loads(msg.payload)
    # print(json.dumps(parsedJSON, indent=4))	# Uncomment this to fill your terminal screen with JSON
    saveToDB(parsedJSON)


def on_subscribe(mqttc, obj, mid, granted_qos):
    print("\nSubscribe: " + str(mid) + " " + str(granted_qos))


def jetzt():
    return str(time.localtime().tm_year) + "." + str(time.localtime().tm_mon).zfill(2) + "." + str(
        time.localtime().tm_mday).zfill(2) + " " + str(time.localtime().tm_hour).zfill(2) + ":" + str(
        time.localtime().tm_min).zfill(2) + ":" + str(time.localtime().tm_sec).zfill(2)

# Hilfsfunktionen
def write_log(file, text):
    # Aktuellen Zeitstempel formatieren
    act_time = jetzt()
    try:
        configfile = open(file, "a")
        configfile.write(act_time + " " + text + "\n")
        configfile.close()
        return 0  # kein Fehler
    except:
        print('Problem beim Schreiben der log-Datei')
        return 1  # Fehler


def info(dat, inhalt):
    print(inhalt)
    write_log(dat, inhalt)


User = "test-kleinere-version@ttn"
Password = "NNSXS.E3K4IIYYKOYJAISIE6VQXL762DQTS733B3RBMSY.QM4NQVL5MTSQO7PBJDHICAVDE5CV4EOG6L7G6WGKUU7EHEE7YVXQ"
theRegion = "EU1"  # The region you are using

datei = 'C:\\Users\\Erich\\Documents\\PycharmProjects\\MQTT\\Feuchte_lesen_DB.log'

info(datei, 'Programmstart')

print("Init mqtt client")
mqttc = mqtt.Client()
print("Assign callbacks")
mqttc.on_connect = on_connect
mqttc.on_subscribe = on_subscribe
mqttc.on_message = on_message
print("Connect")
# Setup authentication from settings above
mqttc.username_pw_set(User, Password)
# IMPORTANT - this enables the encryption of messages
mqttc.tls_set()  # default certification authority of the system
try:
    temp = mqttc.connect(theRegion.lower() + ".cloud.thethings.network", 8883, 60)
except:
    print('Connect geht nicht: ')

print("And run forever")
mqttc.loop_forever(10)  # seconds timeout / blocking time
