#!/usr/bin/env python
import flask
from flask import Flask, render_template, Response, request, make_response
import time
import io
from pymongo import MongoClient
import pymongo
import datetime
import bson
import matplotlib.dates as mdates
# es geht auch ohne Bildschirm
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

app = Flask(__name__)

rpyc_fehlt = True
verbindung = None
startJahr = 2023
startMonat = 1
startTag = 11
startStunde = 18
endeJahr = 2023
endeMonat = 1
endeTag = 31
endeStunde = 18
aufloes = 3  # Stunden für die Auswertung des Regens
faktr = 1
faktl = 1
logName = '/home/erich/Dokumente/Zeige_Feuchte.log'
diffZeit = datetime.timedelta(hours=1)  # gleicht Zeitzone aus, ggf. Sommerzeit einbauen
skala = 2   # mm gebraucht für Regenmenge

@app.route('/', methods=['POST', 'GET'])
@app.route('/index', methods=['POST', 'GET'])
def FeuchteZeigen():
    """Daten vom Server holen."""
    global startJahr, startMonat, startTag, startStunde, endeJahr, endeMonat, endeTag, endeStunde, faktl, faktr
    if request.method == 'POST':
        startJahr = int(request.form['startJahr'])
        startMonat = int(request.form['startMonat'])
        startTag = int(request.form['startTag'])
        startStunde = int(request.form['startStunde'])
        endeJahr = int(request.form['endeJahr'])
        endeMonat = int(request.form['endeMonat'])
        endeTag = int(request.form['endeTag'])
        endeStunde = int(request.form['endeStunde'])
        faktr = int(request.form['faktr'])
        faktl = int(request.form['faktl'])

    return render_template('Rohdaten.html', startJahr=startJahr, startMonat=startMonat, startTag=startTag,
                           startStunde=startStunde, endeJahr=endeJahr, endeMonat=endeMonat, endeTag=endeTag,
                           endeStunde=endeStunde, faktl=faktl, faktr=faktr)


@app.route('/regen_feuchte', methods=['POST', 'GET'])
def regen_feuchte():
    """Daten vom Server holen."""
    global startJahr, startMonat, startTag, startStunde, endeJahr, endeMonat, endeTag, endeStunde, aufloes, skala, diffZeit
    if request.method == 'POST':
        startJahr = int(request.form['startJahr'])
        startMonat = int(request.form['startMonat'])
        startTag = int(request.form['startTag'])
        startStunde = int(request.form['startStunde'])
        endeJahr = int(request.form['endeJahr'])
        endeMonat = int(request.form['endeMonat'])
        endeTag = int(request.form['endeTag'])
        endeStunde = int(request.form['endeStunde'])
        aufloes = int(request.form['aufloes'])
        skala = int(request.form['skala'])

    return render_template('Regen_Feuchte.html', startJahr=startJahr, startMonat=startMonat, startTag=startTag,
                           startStunde=startStunde, endeJahr=endeJahr, endeMonat=endeMonat, endeTag=endeTag,
                           endeStunde=endeStunde, aufloes=aufloes, skala=skala)


def write_log(file, text):
    # Aktuellen Zeitstempel formatieren
    act_time = str(time.localtime().tm_year) + "." + str(time.localtime().tm_mon).zfill(2) + "." + str(
        time.localtime().tm_mday).zfill(2) + " " + str(time.localtime().tm_hour).zfill(2) + ":" + str(
        time.localtime().tm_min).zfill(2) + ":" + str(time.localtime().tm_sec).zfill(2)
    try:
        configfile = open(file, "a")
        configfile.write(act_time + " " + text + "\n")
        configfile.close()
        return 0  # kein Fehler
    except:
        return 1  # Fehler


@app.route('/feuchte', methods=['GET'])
def feuchte():
    clientM = MongoClient('localhost', 27017)
    db = clientM.Feuchte
    inhalt = db.Inhalte
    # letzten Wert abfragen
    etwas = inhalt.find_one({'Id': 'Feuchte'}, sort=[('_id', pymongo.DESCENDING )])
    wert = etwas['value']
    clientM.close()
    return str(wert)


@app.route('/regen', methods=['GET'])
def regen():
    global diffZeit
    dbEintrag = ['Regen_lang']
    minuten = flask.request.args.get('minuten')
    clientM = MongoClient('localhost', 27017)
    db = clientM.Feuchte
    inhalt = db.Inhalte
    # zeitliche Auswahl abfragen
    oid = bson.objectid.ObjectId()
    endZeit = datetime.datetime.utcnow()
    end_id = oid.from_datetime(endZeit)
    startZeit = endZeit - datetime.timedelta(minutes=int(minuten))
    start_id = oid.from_datetime(startZeit)
    # zunächst den Zeitbereich extrahieren
    alles = inhalt.find({'$and': [{"_id": {"$gt": start_id}}, {"_id": {"$lt": end_id}}]})
    listAlles = []
    for erge in alles:
        listAlles.append(erge)
    # Dictionary of Lists
    allErg = {}
    allZeit = {}
    for typStrom in dbEintrag:
        allErg[typStrom] = []
        allZeit[typStrom] = []
    for temp in listAlles:
        for typStrom in dbEintrag:
            try:
                if temp['Id'] == typStrom:
                    allErg[typStrom].append(temp['value'])
                    allZeit[typStrom].append(temp['_id'].generation_time)
            except:
                print('Falsches Feld')
    return(str(allErg[dbEintrag[0]][-1] - allErg[dbEintrag[0]][0]))

@app.route("/roh_diagramm")
def roh_diagramm():
    """Messwerte als Dictionary holen"""
    global startJahr, startMonat, startTag, startStunde, endeJahr, endeMonat, endeTag, endeStunde, diffZeit, tagDict, faktl, faktr

    dbEintrag = ['Temperatur', 'Feuchte', 'Regen', 'Regen_lang', 'Spannung']
    colorDict = {'Temperatur': 'r', 'Feuchte': 'g', 'Regen': 'b', 'Regen_lang': 'c', 'Spannung': 'y'}

    clientM = MongoClient('localhost', 27017)
    db = clientM.Feuchte
    inhalt = db.Inhalte
    # zeitliche Auswahl abfragen
    oid = bson.objectid.ObjectId()
    startZeit = datetime.datetime(startJahr, startMonat, startTag, startStunde, 0, 0) - diffZeit
    if time.localtime(startZeit.timestamp())[8] == 1:  # Sommerzeit erkannt
        diffZeit = datetime.timedelta(hours=2)
    else:
        diffZeit = datetime.timedelta(hours=1)
    start_id = oid.from_datetime(startZeit)
    endZeit = datetime.datetime(endeJahr, endeMonat, endeTag, endeStunde, 59, 59) - diffZeit
    end_id = oid.from_datetime(endZeit)
    # begrenze die Anzahl der Tage auf 50, Warning
    if endZeit - startZeit > datetime.timedelta(50):
        print('zu groß')
    # zunächst den Zeitbereich extrahieren
    alles = inhalt.find({'$and': [{"_id": {"$gt": start_id}}, {"_id": {"$lt": end_id}}]})
    listAlles = []
    for erge in alles:
        listAlles.append(erge)
    # Dictionary of Lists
    allErg = {}
    allZeit = {}
    for typStrom in dbEintrag:
        allErg[typStrom] = []
        allZeit[typStrom] = []
    for temp in listAlles:
        for typStrom in dbEintrag:
            try:
                if temp['Id'] == typStrom:
                    allErg[typStrom].append(temp['value'])
                    allZeit[typStrom].append(temp['_id'].generation_time)
            except:
                print ('Falsches Feld')
    # ggf. weniger Werte ins Diagramm übernehmen
    punktZahl = 150
    # z.B. durch ausdünnen der Arrays
    redukt = int(len(allErg['Temperatur']) / punktZahl)  # oBdA
    if redukt > 1:
        for typStrom in dbEintrag:
            allErg[typStrom][:] = allErg[typStrom][::redukt]
            allZeit[typStrom][:] = allZeit[typStrom][::redukt]

    fig = plt.figure(num=1, figsize=(8, 7), dpi=150)
    tageFmt = mdates.DateFormatter('%H:00 %d.%m.')
    ax = fig.add_subplot(111)
    ergArr = []
    for treffer in dbEintrag:
        zeitDia = []
        wertErtrag = []
        for i in range(1, len(allErg[treffer])):
            zeit = allZeit[treffer][i]  # Zulu-Zeit, Zeitzone beachten
            zeitAnz = zeit + diffZeit  # korrigiert für Anzeige
            zeitDia.append(zeitAnz)
            wert = allErg[treffer][i]
            if treffer == 'Spannung':
                wert = (wert - 1900)/7
            if treffer == "Regen":
                wert = wert - allErg[treffer][0]    # führt nach Neustart zu negativen Werten !!
                wert = wert / faktr
            if treffer == "Regen_lang":
                wert = wert - allErg[treffer][0]
                wert = wert / faktl
            wertErtrag.append(wert)
        ax.plot(zeitDia, wertErtrag, label=treffer, color=colorDict[treffer])
    ax.legend(loc=10)
    ax.set_ylabel('Einheiten')
    ax.grid(True)
    ax.xaxis.set_major_formatter(tageFmt)
    fig.autofmt_xdate()
    png_output = io.BytesIO()
    # speichere das Diagramm im PNG-Format in einem Stream
    plt.savefig(png_output, format='png')
    plt.close()
    png_output.seek(0)

    # file = open("/home/pi/Documents/PycharmProjects/StromZaehler/Erg/test.png", 'wb')
    # file.write(png_output.getvalue())
    # file.close()
    figdata_png = png_output.getvalue()
    png_output.close()
    response = make_response(figdata_png)
    del figdata_png, png_output, listAlles, zeitDia, wertErtrag
    response.headers['Content-Type'] = 'image/png'
    clientM.close()
    return response


@app.route("/auswertung_diagramm")
def auswertung_diagramm():
    global startJahr, startMonat, startTag, startStunde, endeJahr, endeMonat, endeTag, endeStunde, diffZeit, aufloes

    dbEintrag = ['Temperatur', 'Feuchte', 'Regen_lang']
    colorDict = {'Temperatur': 'r', 'Feuchte': 'g', 'Regen_lang': 'c'}

    clientM = MongoClient('localhost', 27017)
    db = clientM.Feuchte
    inhalt = db.Inhalte
    # zeitliche Auswahl abfragen
    oid = bson.objectid.ObjectId()
    startZeit = datetime.datetime(startJahr, startMonat, startTag, startStunde, 0, 0) - diffZeit
    if time.localtime(startZeit.timestamp())[8] == 1:  # Sommerzeit erkannt
        diffZeit = datetime.timedelta(hours=2)
    else:
        diffZeit = datetime.timedelta(hours=1)
    start_id = oid.from_datetime(startZeit)
    endZeit = datetime.datetime(endeJahr, endeMonat, endeTag, endeStunde, 59, 59) - diffZeit
    end_id = oid.from_datetime(endZeit)
    if endZeit < startZeit:
        print('falscher Zeitbereich')
        return 'falscher Zeitbereich'
    # begrenze die Anzahl der Tage auf 50, nur interne Warning
    if endZeit - startZeit > datetime.timedelta(50):
        print('zu groß')
    # zunächst den Zeitbereich extrahieren
    alles = inhalt.find({'$and': [{"_id": {"$gt": start_id}}, {"_id": {"$lt": end_id}}]})
    listAlles = []
    for erge in alles:
        listAlles.append(erge)
    # Dictionary of Lists
    allErg = {}
    allZeit = {}
    for typStrom in dbEintrag:
        allErg[typStrom] = []
        allZeit[typStrom] = []
    for temp in listAlles:
        for typStrom in dbEintrag:
            try:
                if temp['Id'] == typStrom:
                    allErg[typStrom].append(temp['value'])
                    allZeit[typStrom].append(temp['_id'].generation_time)
            except:
                print('Falsches Feld')
    # ggf. weniger Werte ins Diagramm übernehmen
    punktZahl = 150
    # z.B. durch ausdünnen der Arrays
    # ist das bei der Differenzberechnung gut?  !!!
    redukt = int(len(allErg['Temperatur']) / punktZahl)  # oBdA
    if redukt > 1:
        for typStrom in dbEintrag:
            allErg[typStrom][:] = allErg[typStrom][::redukt]
            allZeit[typStrom][:] = allZeit[typStrom][::redukt]

    fig = plt.figure(num=1, figsize=(8, 7), dpi=150)
    tageFmt = mdates.DateFormatter('%H:00 %d.%m.')
    ax = fig.add_subplot(111)
    for treffer in dbEintrag:
        zeitDia = []
        wertErtrag = []
        if treffer == 'Regen_lang':
            ax2 = ax.twinx()
            for i in range(1, len(allErg[treffer])):
                zeit = allZeit[treffer][i-1]  # Zulu-Zeit, Zeitzone beachten
                zeitAnz = zeit + diffZeit  # korrigiert für Anzeige
                zeitDia.append(zeitAnz)
                wert = allErg[treffer][i]
                wertErtrag.append(wert)
            try:
                anfang = zeitDia[0]
            except:
                print('kein Index')
                return
            delta_tg = zeitDia[-1] - zeitDia[0]
            anz = int((delta_tg.total_seconds() / 3600) / aufloes)
            delta_t = datetime.timedelta(hours=aufloes)
            plotZeit = []
            plotWert = []
            #testRes = []
            wertAlt = wertErtrag[0]
            for i in range(0, anz):
                res = next(x for x, val in enumerate(zeitDia) if val >= (anfang + (i + 1) * delta_t))
                #estRes.append(res)
                plotWert.append(wertErtrag[res] - wertAlt)
                wertAlt = wertErtrag[res]
                plotZeit.append(zeitDia[res-1])
            ax2.bar(plotZeit, plotWert, linewidth=2, width=0.04*aufloes, align='center', edgecolor=colorDict[treffer],
                    color='none')
            ax2.tick_params('y', color=colorDict[treffer])
            ax2.set_ylabel('Regenmenge in mm', color=colorDict[treffer])
            ax2.tick_params(axis='y', colors=colorDict[treffer])
            ax2.set_ylim([0, skala])
        else:
            for i in range(1, len(allErg[treffer])):
                zeit = allZeit[treffer][i]  # Zulu-Zeit, Zeitzone beachten
                zeitAnz = zeit + diffZeit  # korrigiert für Anzeige
                zeitDia.append(zeitAnz)
                wert = allErg[treffer][i]
                wertErtrag.append(wert)
            ax.plot(zeitDia, wertErtrag, label=treffer, color=colorDict[treffer])
    ax.legend(loc=10)   # Mitte
    ax.set_ylabel('Temperatur in °C             Feuchte in %')
    ax.set_ylim([-20, 100])
    ax.grid(True)
    ax.xaxis.set_major_formatter(tageFmt)
    fig.autofmt_xdate()
    png_output = io.BytesIO()
    # speichere das Diagramm im PNG-Format in einem Stream
    plt.savefig(png_output, format='png')

    plt.close()
    png_output.seek(0)
    # file = open("/home/pi/Documents/PycharmProjects/StromZaehler/Erg/test.png", 'wb')
    # file.write(png_output.getvalue())
    # file.close()
    figdata_png = png_output.getvalue()
    png_output.close()
    response = make_response(figdata_png)
    del figdata_png, png_output, listAlles, zeitDia, wertErtrag, plotZeit, plotWert
    response.headers['Content-Type'] = 'image/png'
    clientM.close()
    return response


if __name__ == '__main__':
    text = 'Flask wird gestartet '
    print(text)
    write_log(logName, text)

    app.run(host='0.0.0.0', port=8080, debug=False, threaded=True)
