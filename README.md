# Steuerung der Beregnung der Tennispl�tze bei der TG-K�fertal in Mannheim 

Das Dokument beschreibt eine Python-Software, die die Beregnung der Tennispl�tze bei der TG-K�fertal automatisiert.

*Die Steuerung erfolgt �ber die auf dem Tennisplatz vorhandenen Taster an den Pl�tzen und im Aufenthaltsraum. Die dort verbauten Leuchtdioden geben R�ckmeldung �ber den Status der Steuerung.
Parallel dazu kann der Status des Systems im Bereich des Wlans der TG-K�fertal in einem Browser angezeigt werden. Nach Login eines angelegten Benutzers kann dieser auch �ber den Browser neben allen Steuerungsfunktionen der Taster auch weitere Einstellungen am System vornehmen.*

---


## Verwendete Hardware

Verwendet wird ein Raspberry Pi (hier: 4) mit Ein- /Ausgabebaugruppen der Firma Horter & Kalb (https://www.horter-shop.de/de/97-i2c-hutschienen-module) auf 24V-Basis. Die Ein- und Ausgabebaugruppen sind vom Raspberry Pi durch Optokoppler galvanische getrennt:

2*Baugruppe I2EOK f�r die 14 Eing�nge der Taster.

2*Baugruppe I2AT f�r die 10 Relais, die die Magnetventile ansteuern.

2*Baugruppe I2AOK f�r die 14 LEDs (umgebaut auf npn-Ausgang).

Weiterhin werden ein I2C-Repeater, Busverbinder und Montagerahmen gebraucht.

---

## Sytem aufsetzen

Ein Raspberry Pi mit Hostname *tg-beregnung* wird mit User *pi* aufgesetzt und ein gutes Passwort vergeben. Die Schnittschellen SSH und I2C werden eingeschaltet. Python (es funktioniert z.B. Python 3.73 mit hier abgelegter *requirements.txt* Datei) muss eingerichtet sein und braucht 
*flask-socketio* (*Flask* sollte bereits installiert sein) und Module f�r den User 'sudo', z.B. *sudo pip3 install eventlet*.

XRDP wird mit *sudo apt-get install xrdp* installiert. Dar�ber wird das System 'headless' gesteuert. 

F�r den Watchdog wird eine Ram-Disk */mnt/RAMDisk/* gebraucht:
https://www.kriwanek.de/index.php/de/raspberry-pi/265-ram-disk-auf-raspberry-pi-einrichten

Die Funktion des Watchdogs ist hier beschrieben:
https://www.gieseke-buch.de/raspberrypi/eingebauten-hardware-watchdog-zur-ueberwachung-nutzen

Die Datei: */mnt/RAMDisk/watch.log* wird verwendet. Konfiguriert wird der Watchdog 
in /etc/watchdog.conf mit:
change = 900  # entspricht 15min bis zum Ausl�sen.

**Achtung:** nach dem Aktivieren muss die Datei regelm��ig beschrieben werden. Sonst bootet das System immer wieder neu. Daher den Watchdog erst ganz zum Schluss aktivieren, wenn das Steuerungsprogramm l�uft.

Mit folgendem Inhalt von */etc/rc.local* wird das Beregnungsprogramm automatisch beim Neustart des Systems gestartet:  
```
cd /home/pi/beregnungssteuerung-oe
python3 Start.py
exit 0
```
  
---


## Installation der Software

1. Nach dem Aufsetzen eines neuen Systems: Download repository z.B. durch `git clone ...` aus diesem repository vom Verzeichnis `/home/pi` aus.  

2. Um �nderungen einzuspielen: `git pull ...` in `/home/pi/beregnungssteuerung-oe`  

---


## Funktionsbeschreibung

1. **Manueller Betrieb mit einer Taste pro Platz**
Taster pro Platz dr�cken: wenn Licht *aus* und kein anderer Platz l�uft und kein Automatikbetrieb  
-> Beregnung f�r eine Zeit *tEin* f�r diesen Platz eingeschaltet (manuell ein)   
Taster wieder dr�cken: wenn Beregnung manuell ein f�r diesen Platz  
-> Beregnung aus  
2. Bedeutung der weiteren 4 Tasten:  
**Rundlauf** startet Beregnung nach vorgew�hltem Muster, nochmaliges Dr�cken beendet Rundlauf  
**Sperre 1-4** schaltet das Muster so, dass Platz 1-4 nicht beregnet werden, Led leuchtet, nochmaliges Dr�cken beendet die Sperre   
**Sperre 5-10** schaltet das Muster so, dass Platz 5-10 nicht beregnet werden, nochmaliges Dr�cken beendet die Sperre  
**Sperre** sperrt den *Automatikbetrieb* zu vorgew�hlten Zeiten z.B. bei zu nassen Pl�tzen oder weil wichtige Spiele anliegen  
3. **Rundlauf** nach Muster, nacheinander mit *tEin = 1 bis 10 min* mit ca. 10s Sperre (= t_sperre) zwischen den Pl�tzen, Priorit�t vor Einzelbew�sserung  
4. **Automatikbetrieb** zu vorgew�hlten Zeiten wird der *Rundlauf* ausgel�st   

5. **Sperre Beregnung** vom Web-Interface f�r alle angemeldeten User. Nach Sperren der Pl�tze sind auch keine Einzelbew�sserungen m�glich. Modus wird durch Button *Eingabe Sperre* ein- und ausgeschaltet.  Button leuchtet dann Orange. Nun k�nnen einzelne Pl�tze durch Anklicken gesperrt werden. Die Pl�tze leuchten nun zur Best�tigung orange. Durch ein weiteres Tippen auf den Button wird in den normalen Beregnungsmodus zur�ckgeschaltet. Gesperrte Pl�tze werden unterhalb des Buttons wei� statt blau angezeigt. 
6. Ansteuerung / Bedienung der Steuerung �ber **Web-Interface**, z.B. zur Eingabe von Zeiten bei 4.  
- Taster als Command-Buttons (das zweite Dr�cken hat eine andere Bedeutung wie das erste Dr�cken) und Eigenschaft des Command-Buttons (z.B. Farbe) �ndert sich wie Led bei der Steuerung vor Ort  
- Dynamische Aktualisierung der Web-Seite  
- Layout der Pl�tze ist abgebildet  
- Gesperrte Pl�tze werden angezeigt: wei� -> gesperrt, hellblau -> kann bew�ssert werden  
7. Zus�tzliche Funktionen zur Rasenberegnung (nur �ber das Web-Interface):
- **Rasen Rundlauf** entsprechend 3. bei der Platzberegnung
- **Rasen 1** bis **Rasen 3** manueller Betrieb eines einzelnen Regners
- Eingabe der Zeiten f�r Automatikbetrieb wie bei der Platzberegnung mit eigenen Werten f�r den Rasen
8. **Entprellung**   
Erst nachdem ein Tastersignal eine Zeit *tPrell* eingeschaltet war, wird eine Aktion ausgel�st  
Logiksignale lesen, wenn Differenz zu altem Signal, wird ein zugeh�riger Timer gestartet und der neue Wert �bernommen  
Wenn *tPrell* auf einem Wert abgelaufen ist wird das zugeh�rige Logiksignal dieses Tasters gesetzt -> Aktionsblock ausf�hren.  
9. **Watchdog** startet das System bei St�rung des Rundlaufs neu.
10. System startet nach dem Reboot mit manuellem Betrieb.
11. **User-Login** f�r die Steuerung von der Web-Oberfl�che und zur Eingabe von Parametern
---


## Aufbau der Software  

Steuerung als 'Rundl�ufer': Polling der Eing�nge, setzen von Markern und Zeitgebern, Auswertung von Markern und Zeitgebern, Schalten der Ausg�nge, Watchdog zur�cksetzen.  
  
Parallel dazu ein Web-Interface mit Flask: statische Variable koordinieren die Zusammenarbeit mit dem Steuerungsmodul.  

**Beregnung** ist das Modul, das verschiedene Klassen mit ihren Methoden zur Verf�gung stellt.
Mit `app = br.create_app()` aus *\_\_init\_\_.py* wird folgendes angesto�en:  
- bereite den Flask-Server vor und stellt die Pfade des Servers zur Verf�gung  
- starte die Hauptschleife der Steuerung  
- starte Datenbank   
- starte Autorisierung
```
br.socketio.run(app, host='0.0.0.0', port=80, debug=False)
```
startet den Flask-Server in *Start.py* �ber socketio f�r die �bermittlung des Status auf alle angeschlossenen Browser.  

*\_\_init\_\_.py* stellt weitere statische Methoden zur Verf�gung, die von �berall her aufgerufen werden k�nnen.  

Die zentrales Steuerung ist in der Klasse **Hauptschleife** als 'Rundl�ufer' realisiert. Sie l�uft in einem eigenen Thread.  
  
Die Anlage wird in ihrem Aufbau und mit wichtigen Parametern in der Klasse **AnlagenKonfiguration** beschrieben. Der Zustand der Anlage steht mit der Klasse **AnlagenStatus** zur Verf�gung.  

Die Beregnung im Einzelnen wird mit Methoden aus der Klasse **Regen** gesteuert.  

Sie greift auf statische Ein- und Ausgabefunktionen auf dem I2C-Bus aus **I2Cio.py** zur�ck.  
 
Periodische oder einmalige Zeitgeber werden von der Klasse **Zeitgeber** zur Verf�gung gestellt.  

Eintragungen in der Log-Datei erolgen aus der Klasse **LogDatei** heraus und die Watch-Dog-Datei wird aus **WatchDog** heraus beschrieben.
 

---


## Datenbank der User initialisieren und neue User anlegen 

In */home/pi/beregnungssteuerung-oe/* steht die Datenbank *flaskr.sqlite* mit den User-Daten im instance-Ordner. Vor dem Zugriff sind die Environment-Variablen zu setzen:
```
export FLASK_APP=Beregnung:create_app
export FLASK_ENV= production
```
Die Datenbank der User ggf. neu aufbauen (**nur einmal!**):
```
flask init-db  
```
Einen User mit Name: *<name>* und Passwort: *<passwd>* eingeben:  
```
flask user <name> <passwd>
```


---
