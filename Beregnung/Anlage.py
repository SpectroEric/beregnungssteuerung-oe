# -*- coding: utf-8 -*-
from datetime import time


class AnlagenKonfiguration:
    datei = '/home/pi/Steuerung.log'
    dateiWatch = '/mnt/RAMDisk/watch.log'
    performance = False     # Performance-Messung der Hauptschleife: True eingeschaltet
    debugprint = False      # printer deaktiviert: ..., False)  aktiviert: ..., True)
    nPlaetze = 10
    tPrell = .5        # Entprell-Zeit in sec, später 0.5sec
    tWart = 5          # Sperrzeit für einen Eingang, nächste Aktion von dieser Taste erst nach abgelaufener Zeit
    t_sperre = 10      # in sec, Pause zwischen zwei Schaltungen von Magnetventilen
    watch_zeit = 60    # Watch-Dog-Zeit
    wiederAuto = 120   # nach 120s wird nach der nächsten Zeit für die Automatik gesucht
    nameTaster = ['Start', 'Sperre 1-4', 'Sperre 5-10', 'Sperre', 'Eingabe Sperre', "Rasen Rundlauf"]    # zusätzliche Taster
    nTaster = len(nameTaster)
    adresseLesen2 = 0x3b   # Hardware-Adressen der I2C-Baugruppen
    adresseLesen1 = 0x3e
    adresseSchreiben1 = 0x22
    adresseSchreiben2 = 0x21
    adresseSchreibAC1 = 0x39
    adresseSchreibAC2 = 0x3d
    bitsProBaugruppe = 8   # Bits pro Baugruppe
    einAdressen = [adresseLesen1, adresseLesen2]  # [Adressen der Baugruppen, ..]
    nBits = bitsProBaugruppe * len(einAdressen) + 5     # vier neue Elemente dazu, sollte für I2C kein Problem sein
    # Liste aller Ausgangsadressen, z.B. zum Löschen
    ausList = [adresseSchreiben1, adresseSchreiben2, adresseSchreibAC1, adresseSchreibAC2]

    # Zuordnung der Ausgangsleitungen zu Adressen
    ausgang = {1: [adresseSchreibAC2, 2], 2: [adresseSchreibAC1, 1], 3: [adresseSchreibAC1, 2],
               4: [adresseSchreibAC1, 3], 5: [adresseSchreibAC1, 4], 6: [adresseSchreibAC1, 5],
               7: [adresseSchreibAC1, 6], 8: [adresseSchreibAC1, 7], 9: [adresseSchreibAC2, 0],
               10: [adresseSchreibAC2, 1],
               18: [adresseSchreibAC2, 3], 19: [adresseSchreibAC2, 4], 20: [adresseSchreibAC2, 5], 21: [adresseSchreibAC2, 6]}

    # Zuordnung der Led-Leitungen zu Adressen
    Led = {1: [adresseSchreiben1, 0], 2: [adresseSchreiben1, 1], 3: [adresseSchreiben1, 2], 4: [adresseSchreiben1, 3],
           5: [adresseSchreiben1, 4], 6: [adresseSchreiben1, 5], 7: [adresseSchreiben1, 6], 8: [adresseSchreiben1, 7],
           9: [adresseSchreiben2, 0], 10: [adresseSchreiben2, 1],
           nameTaster[0]: [adresseSchreiben2, 3], nameTaster[1]: [adresseSchreiben2, 4],
           nameTaster[2]: [adresseSchreiben2, 5], nameTaster[3]: [adresseSchreiben2, 6],
           nameTaster[4]: [adresseSchreiben2, 7]}

    # Zuordnung der Eingangsleitungen zu Tastern
    taster = {nameTaster[0]: 11, nameTaster[1]: 12, nameTaster[2]: 13, nameTaster[3]: 14, nameTaster[4]: 15, nameTaster[5]: 16}


class AnlagenStatus:
    anlageStatus = [False] * AnlagenKonfiguration.nBits     # bildet den Status der Leds ab
    anlageStatus[14] = True         # nach der Initialisierung ist die Automatik gesperrt
    aktion = [False] * AnlagenKonfiguration.nBits           # True: Aktion auf Eingang: index()+1
    muster = [True] * AnlagenKonfiguration.nPlaetze         # Plätze für Rundlauf, True: werden beregnet, False: nicht
    autoZeit = [time.fromisoformat('12:00'),   time.fromisoformat('22:00')]     # als Liste der Uhrzeiten angelegt
    tRegen = 3*60             # 1 bis 10min Beregnungszeit
    autoRasen = [time.fromisoformat('02:00')]     # als Liste der Uhrzeiten angelegt
    tRasen = 7*60             # 1 bis 10min Beregnungszeit
    t_noch = 0                  # Countdown der Beregnungszeit + Sperre
    register = False             # True: Registrierung über Webseite möglich

    @staticmethod
    def muster_schalten(nr: int):
        # Muster verändern als Umschalter
        if AnlagenStatus.muster[nr - 1]:
            AnlagenStatus.muster[nr - 1] = False
        else:
            AnlagenStatus.muster[nr - 1] = True

    @staticmethod
    def muster_testen(von: int, bis: int):
        # Muster testen, von: Platznummer, bis: Platznummer
        erg = AnlagenStatus.muster[von - 1]
        for i in range(von, bis):
            erg = erg or AnlagenStatus.muster[i]
        return not erg

if __name__ == "__main__":
    tg = AnlagenStatus
    tk = AnlagenKonfiguration
    print(tg.anlageStatus)
    print(tg.aktion)
    print(tg.muster)
    print(tk.nBits)
    print(tk.adresseSchreibAC2)

