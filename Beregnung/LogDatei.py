import time
# from Beregnung import Zeitgeber


class LogDatei:
    # Meldungen in Log-Datei speichern
    def __init__(self, fname):
        self.fname = fname    # Dateiname

    def write_log(self, text):
        # Aktuellen Zeitstempel formatieren
        act_time = str(time.localtime().tm_year) + "." + str(time.localtime().tm_mon).zfill(2) + "." + str(
            time.localtime().tm_mday).zfill(2) + " " + str(time.localtime().tm_hour).zfill(2) + ":" + str(
            time.localtime().tm_min).zfill(2) + ":" + str(time.localtime().tm_sec).zfill(2)
        try:
            configfile = open(self.fname, "a")
            configfile.write(act_time + " " + text + "\n")
            configfile.close()
            return 0  # kein Fehler
        except:
            return 1  # Fehler


if __name__ == "__main__":
    from Beregnung import Zeitgeber

    zeit1 = Zeitgeber.Zeitgeber(10, True, True)
    log = LogDatei('/home/pi/test.log')
    while True:
        if zeit1.check():
            print('eine Meldung')
            print(log.write_log('eine Meldung'))

