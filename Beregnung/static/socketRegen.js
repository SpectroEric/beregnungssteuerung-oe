// url of server
var url = 'http://' + document.domain + ':' + location.port  ;
// Abbildung der Plätze auf Zeilen und Spalten der Tabelle unterhalb des Command-Buttons "Eingabe Sperre"
var convert = [[1,0], [0,0], [1,1], [0,1], [2,3], [1,3], [0,3], [2,4], [1,4], [0,4]]

function load_data() {
    var url_load = url + '/data' ;
    $.ajax({ url:     url_load,
             success: function(data) {
                          display_data_zeit(data);
                          // schalte zwischen den beiden Display-Modi um ("Eingabe Sperre" aktiv oder nicht)
                          if (data.format[15])
                                display_data_muster(data);
                          else
                                display_data_schalter(data);
                      },
    });
    return true;
}
function display_data_zeit(data) {   // nimmt die Änderungen an der Webseite vor
    if (data) {      // if there is data
        // update the contents of several HTML divs via jQuery
        //$('#help').html(data.zeit);     // unteres Feld wird gefüllt
        $('#fehler').html(data.fehler);     // oberes Feld wird gefüllt
        //document.getElementById('fehler').value = data.fehler;   // Eingabefeld wird aktualisiert
        document.getElementById('ein').value = data.zeit;   // Eingabefeld wird aktualisiert
        document.getElementById('einregen').value = (Number(data.regenzeit) / 60).toString();    // Eingabefeld wird aktualisiert
        document.getElementById('einauto').value = data.zeitrasen;   // Eingabefeld wird aktualisiert
        document.getElementById('einrasen').value = (Number(data.rasenzeit) / 60).toString();    // Eingabefeld wird aktualisiert
    }
}
function display_data_schalter(data) {   // nimmt die Änderungen an der Webseite vor
    if (data) {      // if there is data
        // nur zeigen, wenn ein Platz gesperrt ist
        if (data.muster.includes(false))
            // Tabelle füllen
            for (var i=0; i<10; i++)
                if (data.muster[i]){
                    changeColor("plaetze", convert[i][0], convert[i][1], "#CCFFFF");
                } else {
                    changeColor("plaetze", convert[i][0], convert[i][1], "white");
                }
        else      // sonst grau füllen
        {
            for (var i=0; i<10; i++)
                    changeColor("plaetze", convert[i][0], convert[i][1], "#eeeeee");
        }
        var skip = 0;   // fehlende Elemente werden übersprungen
        for(var i=0; i<21; i++ ){
            var element = document.getElementById(i.toString());
            if (element == null){
                skip = 1;
            } else {
                var temp = element.style;
            }
            if (skip == 0) {        // Hintergrundfarben werden entsprechend des Bool-Arrays data.format eingestellt
                if (data.format[i]) { temp.backgroundColor = "rgb(82, 177, 255)"} else { temp.backgroundColor = 'white'};
             }
            else {
                skip = 0;
            }
        }
        // Coundown der Zeiten ausgeben
        $('#countdown').html(data.t_noch);     // oberes Feld wird gefüllt

    }
}
function display_data_muster(data) {   // nimmt die Änderungen an der Webseite vor
    if (data) {      // if there is data
        // Taster einfärben
        var i = 15;
        var temp = document.getElementById(i.toString()).style;
        if (data.format[i]) { temp.backgroundColor = "rgb(255, 177, 20)"} else { temp.backgroundColor = 'white'};
        var skip = 0;   // fehlende Elemente werden übersprungen
        for(var i=0; i<10; i++ ){
            try {
                var temp = document.getElementById(i.toString()).style;
            }
            catch(e){
                console.assert(false, 'i= ', i, ' Meldung:  ', e);
                skip = 1;
            }
            if (skip == 0) {        // Hintergrundfarben werden entsprechend des Bool-Arrays data.muster eingestellt
                if (data.muster[i]) { temp.backgroundColor = 'white'} else { temp.backgroundColor = "rgb(255, 177, 20)"};
              }
            else {
                skip = 0;
            }
        }
    }
}
function changeColor(id, row, cell, color) {
  var x = document.getElementById(id).rows[row].cells;
  x[cell].style.backgroundColor = color;
}
function changeContent(id, row, cell, content) {
  var x = document.getElementById(id).rows[row].cells;
  x[cell].innerHTML = content;
}
$(document).ready(function() {
    // Connect to the Socket.IO server.
    var socket = io.connect(url);
    // Event handler for new connections.
    socket.on('update_zeit', function (data) {
        display_data_zeit(data);
    })
    socket.on('update_schalter', function (data) {
        display_data_schalter(data);
    })
    socket.on('update_muster', function (data) {
        display_data_muster(data);
    })
    load_data();
    var  myform = document.getElementById('einregen');
    myform.onblur = (function () {
        var  error = false;
        var inhalt = Number(document.getElementById('einregen').value);
        if (inhalt > 10){
            alert('Beregnungszeiten > 10min sind nicht erlaubt');
        }
        if (inhalt < 0.3){
            alert('Beregnungszeiten < 0.3min sind nicht erlaubt');
        }
    });
    $(window).keydown(function(event){  // Enter-Taste wird ausgeschaltet, nur 'Submit' funktioniert
    if(event.keyCode == 13) {
        event.preventDefault();
        return false;
        }
    });
})