// url of server
var url = 'http://' + document.domain + ':' + location.port  ;

function load_data() {
    var url_load = url + '/data' ;
    $.ajax({ url:     url_load,
             success: function(data) {
                          display_data(data);
                      },
    });
    return true;
}
function display_data(data) {   // nimmt die Änderungen an der Webseite vor
    if (data) {      // if there is data
        // update the contents of several HTML divs via jQuery
        $('#help').html(data.zeit);     // unteres Feld wird gefüllt
        $('#fehler').html(data.fehler);     // oberes Feld wird gefüllt
        document.getElementById('ein').value = data.zeit;   // Eingabefeld wird aktualisiert
        document.getElementById('einregen').value = (Number(data.regenzeit) / 60).toString();    // Eingabefeld wird aktualisiert

        var skip = 0;   // fehlende Elemente werden übersprungen
        for(var i=0; i<16; i++ ){
            try {
                var temp = document.getElementById(i.toString()).style;
 //               console.assert(false, temp.backgroundColor, ' ', skip);
            }
            catch(e){
                console.assert(false, 'i= ', i, ' Meldung:  ', e);
                skip = 1;
            }
            if (skip == 0) {        // Hintergrundfarben werden entsprechend des Bool-Arrays data.format eingestellt
                if (data.format[i]) { temp.backgroundColor = "rgb(82, 177, 255)"} else { temp.backgroundColor = 'white'};
  //              console.assert(false, 'neue Farbe:  ', temp.backgroundColor);
            }
            else {
                skip = 0;
            }
        }
    }
}
$(document).ready(function() {
    // Connect to the Socket.IO server.
    var socket = io.connect(url);
    // Event handler for new connections.
    socket.on('update', function (data) {
        display_data(data);
    })
    load_data();
    var  myform = document.getElementById('einregen');
    myform.onblur = (function () {
        var  error = false;
        var inhalt = Number(document.getElementById('einregen').value);
        if (inhalt > 10){
            alert('Beregnungszeiten > 10min sind nicht erlaubt');
        }
        if (inhalt < 0.3){
            alert('Beregnungszeiten < 0.3min sind nicht erlaubt');
        }
    });
    $(window).keydown(function(event){  // Enter-Taste wird ausgeschaltet, nur 'Submit' funktioniert
    if(event.keyCode == 13) {
        event.preventDefault();
        return false;
        }
    });
})